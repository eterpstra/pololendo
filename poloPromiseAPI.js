'use strict';

// Dependencies
const crypto  = require('crypto');
const request = require('request');
const nonce   = require('nonce')();

// Constants
const version         = '0.0.5';
const PUBLIC_API_URL  = 'https://poloniex.com/public';
const PRIVATE_API_URL = 'https://poloniex.com/tradingApi';
const USER_AGENT      = `poloniex.js ${version}`;
const STRICT_SSL      = true;

// Secrets
let key, secret;

const getPrivateHeaders = function(parameters){
    var paramString, signature;

    if (!key || !secret){
        throw 'Poloniex: Error. API key and secret required';
    }

    // Sort parameters alphabetically and convert to `arg1=foo&arg2=bar`
    paramString = Object.keys(parameters).map(function(param){
        return encodeURIComponent(param) + '=' + encodeURIComponent(parameters[param]);
    }).join('&');

    signature = crypto.createHmac('sha512', secret).update(paramString).digest('hex');

    return {
        Key: key,
        Sign: signature
    };
};

// Make Request
const makeRequest = (options, callback) => {
    if (!('headers' in options)) {
        options.headers = {};
    }

    options.headers['User-Agent'] = USER_AGENT;
    options.json = true;
    options.strictSSL = STRICT_SSL;

    return new Promise((resolve, reject) => {
        request(
            options,
            (err, response, body) => {
                if(err) {
                    return reject(err);
                }
                resolve(body);
            }
        );
    });
};


// Public Methods
let publicAPIMethods = new Map([
    ['returnTicker',[]],
    ['return24Volume',[]],
    ['returnOrderBook',['currencyPair','depth']],
    ['returnTradeHistory',['currencyPair','start','end']],
    ['returnChartData',['currencyPair','start','end','period']],
    ['returnCurrencies',[]],
    ['returnLoanOrders',['currency']]
]);

const publicRequest = (command, parameters) => {
    parameters.command = command;

    let options = {
        method: 'GET',
        url: PUBLIC_API_URL,
        qs: parameters
    };

    return makeRequest(options);
};

// Private Methods
let privateAPIMethods = new Map([
    ['returnBalances',[]],
    ['returnCompleteBalances',[]],
    ['returnDepositAddresses',[]],
    ['returnDepositsWithdrawals',['start','end']],
    ['returnDepositsWithdrawals',['currencyPair']],
    ['returnTradeHistory',['currencyPair','start','end']],
    ['buy',['currencyPair','rate','amount']],
    ['sell',['currencyPair','rate','amount']],
    ['cancelOrder',['orderNumber']],
    ['moveOrder',['orderNumber','rate','amount']],
    ['withdraw',['currency','amount','address']],
    ['returnAvailableAccountBalances',['account']],
    ['returnTradableBalances',[]],
    ['transferBalance',['currency','amount','fromAccount','toAccount']],
    ['returnMarginAccountSummary',[]],
    ['marginBuy',['currencyPair','rate','amount']],
    ['marginSell',['currencyPair','rate','amount']],
    ['getMarginPosition',['currencyPair']],
    ['closeMarginPosition',['currencyPair']],
    ['createLoanOffer',['currency','amount','duration','autoRenew','lendingRate']],
    ['cancelLoanOffer',['orderNumber']],
    ['returnOpenLoanOffers',[]],
    ['returnActiveLoans',[]],
    ['toggleAutoRenew',['orderNumber']]
]);


const privateRequest = (command, parameters) => {
    let options;

    parameters.nonce = nonce();
    parameters.command = command;
    options = {
        method: 'POST',
        url: PRIVATE_API_URL,
        form: parameters,
        headers: getPrivateHeaders(parameters)
    };

    return makeRequest(options);
};

// Streaming API (coming soon)

// API Builder
let makeApi = (api,requestFunction) => {
    let newApi = {};

    api.forEach( (params, command) => {
        newApi[command] = function(params) {
            if( params === undefined ) params = {};
            return requestFunction(command, params);
        }
    });

    return newApi;
};

// Module
module.exports = {
    setKeys: (myKey, mySecret) => {key = myKey; secret = mySecret},
    public: makeApi(publicAPIMethods, publicRequest),
    private: makeApi(privateAPIMethods, privateRequest)
};