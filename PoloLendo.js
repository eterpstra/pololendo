'use strict';

const _ = require('lodash');
const moment = require('moment');
const stats = require('stats-lite');
const prettyjson = require('prettyjson');
const Table = require('cli-table');
const Poloniex = require('./poloPromiseAPI');



// ***** THE BOT ********
function PoloLendo(key, secret, currency, minRate) {
    Poloniex.setKeys(key, secret);

    this.minRate = minRate || 0.0005;
    this.currency = currency;
    this.lendingAccountBalance = 0;
    this.openOffers = [];
    this.accountState = {
        TotalLendingBalance: 0,
        TotalOfferBalance: 0,
        MyOpenOffers :[],
        UnusedLendingBalance: 0
    };
}


PoloLendo.prototype.startLending = function startLending() {

    //console.log(`\n\n::=_${this.currency}_=::  ${tStamp()}  ::=_${this.currency}_=::`);

    this.accountState = {
        TotalLendingBalance: 0,
        TotalOfferBalance: 0,
        MyOpenOffers :[],
        UnusedLendingBalance: 0
    };

    Poloniex.private.returnAvailableAccountBalances({ account: 'lending' })
        .then( (result) => {
            if (result['lending']) {
                this.lendingAccountBalance = result['lending'][this.currency] || 0;
                return Poloniex.private.returnOpenLoanOffers();
            } else {
                throw new Error('Could not retrieve lending balance.');
            }
        })
        .then( (result) => {
            this.openOffers = result[this.currency] || [];
            return Poloniex.private.returnActiveLoans();
        })
        .then( (result) => {
            this.activeLoans = result['provided'];
            return this.getLendingAccountState();
        })
        .then( () => this.cancelMyStaleOpenOffers() )
        .then( () => this.getPublicLoanOffers() )
        .then( (offers) => {
            if( this.accountState.UnusedLendingBalance > (this.accountState.TotalLendingBalance * .0075) ) {
                return this.doLoans(offers)
            } else {
                return //console.log('Not enough funds to loan');
            }
        })
        .catch((err) => console.log(err.message));
};

PoloLendo.prototype.getLendingAccountState = function getLendingAccountState() {
    return new Promise((resolve, reject) => {

        // My Account Balance
        if ( this.lendingAccountBalance !== undefined ) {
            this.accountState.UnusedLendingBalance = +this.lendingAccountBalance;
        } else {
            throw new Error('Lending balance is undefined.');
        }

        // My Open Offers
        this.accountState.MyOpenOffers = this.openOffers;
        let openOfferSum = 0;
        if ( this.openOffers.length > 0 ) {
            openOfferSum = _.sumBy(this.openOffers, (offer) => +offer.amount );
        }
        this.accountState.TotalOfferBalance = openOfferSum;

        // My Active Loans
        let activeLoanTotalAmount = fieldTotal(this.activeLoans, 'amount', this.currency);
        let activeLoanFeesAmount = fieldTotal(this.activeLoans, 'fees', this.currency);


        // My Lending Account Value
        let totalLendingBalance = this.accountState.UnusedLendingBalance + openOfferSum + activeLoanTotalAmount + activeLoanFeesAmount;
        //console.log('TOTAL VALUE:', totalLendingBalance );
        this.accountState.TotalLendingBalance = totalLendingBalance;

        resolve();
    });
};

PoloLendo.prototype.cancelMyStaleOpenOffers = function cancelMyStaleOpenOffers() {
    return new Promise((resolve, reject) => {

        let staleOffers = this.accountState.MyOpenOffers.filter( (offer) => {
            return moment.utc(offer.date).isBefore(moment.utc().subtract(30,'minutes'));
        });

        if( staleOffers && staleOffers.length > 0) {
            //console.log(`Cancelling ${staleOffers.length} stale loans.`);
            let cancelLoanOfferPromises = staleOffers.map( offer => Poloniex.private.cancelLoanOffer({orderNumber: offer.id}) );

            Promise.all(cancelLoanOfferPromises)
                .then( results => resolve(results.filter( result => result.success === 1).length) )
                .catch(e => reject(e))
        } else {
            resolve(0);
        }

    });
};

PoloLendo.prototype.getPublicLoanOffers = function getPublicLoanOffers() {
    return new Promise( (resolve, reject) => {

        Poloniex.public.returnLoanOrders({currency:this.currency})
            .then( (result) => resolve(result.offers) )
            .catch( (error) => {
                throw new Error('Could not get Public loan offers.\r\n' + error.message)
            });

    });
};

PoloLendo.prototype.doLoans = function doLoans(offers) {
    return new Promise((resolve, reject) => {

        let myLoanAmountFraction = this.accountState.TotalLendingBalance * .10;
        let myLoanAmount = myLoanAmountFraction > this.accountState.UnusedLendingBalance ? this.accountState.UnusedLendingBalance : myLoanAmountFraction;
        let myRate = .02;

        let topRates = _.take(offers.map((offer) => +offer.rate), 5);
        let stdDev = stats.stdev(topRates);

        if (stdDev < .0002) {
            myRate = topRates[0] - (topRates[0] * 0.03);
        } else {
            // Move 1/2 std dev below the mean of top 5 rates
            myRate = stats.mean(topRates) - (0.5 * stdDev);
        }

        if (myRate >= this.minRate) {
            console.log(`Making ${this.currency} ${myLoanAmount} loan at ${myRate*100}%`);

            Poloniex.private.createLoanOffer({
                currency: this.currency,
                amount: myLoanAmount,
                duration: 2,
                autoRenew: 0,
                lendingRate: myRate
            }).then( (result) => {
                if( result.success ) {
                    resolve();
                } else {
                    reject('Problem returned from loan offer:: ', result.message);
                }
            }).catch( (e) => {
                console.log('Problem making loan offer:: ', e.message);
            });
        } else {
            console.log(`Calculated rate of ${myRate} below min threshold of ${this.minRate}`);
            resolve();
        }

    });
};

module.exports = PoloLendo;


// *************************
// *** UTILITY FUNCTIONS ***
// *************************


function fieldTotal(data, field, currency) {
    let values = _(data)
                .filter( item => item.currency === currency)
                .map( item => +item[field] )
                .valueOf();

    let sum = values.reduce( (lastValue, nextValue) => lastValue + nextValue , 0 );

    return sum;
}

function tStamp() {
    return moment().format('MMMM Do YYYY, h:mm:ss a');
}
