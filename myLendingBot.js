"use strict";

require('dotenv').config();
const md5 = require('md5');
const auth = require('basic-auth');
const server = require('express')();
const PoloLendo = require('./PoloLendo');

const ethBot = new PoloLendo(process.env.KEY, process.env.SECRET, 'ETH', .00055);
const btcBot = new PoloLendo(process.env.KEY, process.env.SECRET, 'BTC', .00100);
const bots = [ethBot, btcBot];

let headerInfo = '';
let separator = '<br/>=============================<br/>';
let initialLendingInfo = [];
let lendingInfo = [];

function startBot(intervalInMinutes) {
    let activeBotIdx = 0;

    headerInfo = `Started Lending: ${new Date().toString()}`;
    bots[activeBotIdx].startLending();
    
    setInterval(() => {
        let currentBot = bots[activeBotIdx];

        if(!initialLendingInfo[activeBotIdx]) {
            initialLendingInfo[activeBotIdx] = `Starting ${currentBot.currency} value: ${currentBot.accountState.TotalLendingBalance}`;
        }

        lendingInfo[activeBotIdx] = `Current ${currentBot.currency} value: ${currentBot.accountState.TotalLendingBalance}`;
        
        activeBotIdx = activeBotIdx < (bots.length - 1) ? activeBotIdx + 1 : 0;
        bots[activeBotIdx].startLending();

    }, 1000 * 60 * intervalInMinutes);
    
}


server.get('/', function (req, res) {
    var credentials = auth(req);

    if (!credentials || credentials.name !== process.env.LOGIN || md5(credentials.pass) !== process.env.PASS) {
        res.statusCode = 401;
        res.setHeader('WWW-Authenticate', 'Basic realm="example"');
        res.end('Access denied')
    } else {
        res.send(`
            ${headerInfo}
            ${separator}
            ${initialLendingInfo.join('<br/>')}
            ${separator}
            ${lendingInfo.join('<br/>')}
        `);
    }

});

server.listen(3030, function () {
    console.log('PoloLendo listening on port 3030!');
    startBot(process.env.INTERVAL);
});
